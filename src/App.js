import React, { useEffect } from 'react'
import 'fontsource-roboto'
import { BrowserRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import Navbar from './containers/Navbar'
import Routes from './containers/Routes'
import SnackbarsInfo from './containers/SnackbarsInfo'
import AddEditTaskModal from './containers/Meetroom/AddEditTaskModal'
import { autoAuthorization } from './store/actions/actionAuthorization'

const App = ({ checkSession }) => {
  useEffect(() => {
    checkSession()
  }, [checkSession])

  return (
    <BrowserRouter>
      <Navbar />
      <Routes />
      <SnackbarsInfo />
      <AddEditTaskModal/>
    </BrowserRouter>
  )
}
const mapDispatchToProps = (dispatch) => ({
  checkSession: () => dispatch(autoAuthorization()),
})

export default connect(null, mapDispatchToProps)(App)
