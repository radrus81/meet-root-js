export function formatDateYMDFromDate(date, format) {
  var curr_date = date.getDate()
  var curr_month = date.getMonth() + 1
  var curr_year = date.getFullYear()
  if (format === 'Y-m-d') {
    return `${curr_year}-${curr_month}-${curr_date}`
  } else if (format === 'd.m.Y') {
    return `${curr_date}.${curr_month}.${curr_year}`
  }
}

export function formatDateYMDFromString(dateString) {
  const arrDate = dateString.split('.')
  return `${arrDate[2]}-${arrDate[1]}-${arrDate[0]}`
}

export function getNameDateWeekFromDate(date){  
  var codeNameWeek = date.getDay()  
  switch(codeNameWeek){
    case 1 : return 'monday';
    case 2 : return 'tuesday';
    case 3 : return 'wednesday';
    case 4 : return 'thursday';
    case 5 : return 'friday';
    case 6 : return 'saturday';
    case 0 : return 'sunday';
    default: return ''
  }
}

export function calcTime(stringCurrentTime){
  var jqdate = `01.01.2021 ${stringCurrentTime}`
  var d = new Date()
  var time = jqdate.replace(/^(\d+)\.(\d+)\./, '$2/$1/')
  d.setTime(Date.parse(time))
  d.setMinutes(d.getMinutes() + 30)
  return d.toLocaleTimeString().substr(0, 5)
}

