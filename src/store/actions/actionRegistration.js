import axios from 'axios'
import { showShackbars } from './actionSnackbarsInfo'
import { checkLoginAndPassword } from './actionAuthorization'

export function userRegistration(email, password) {
  return async (dispatch) => {
    const authData = {
      email,
      password,
      returnSecureToken: true,
    }
    try {
      await axios.post(
        'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyDMTe5vhMZuXWJcZOsgWiqMmIb7CTWr9HM',
        authData
      )
      dispatch(checkLoginAndPassword(email, password))
      dispatch(showShackbars('Registration success', 'success'))
    } catch (error) {
      dispatch(showShackbars(error.response.data.error.message, 'error'))
    }
  }
}
