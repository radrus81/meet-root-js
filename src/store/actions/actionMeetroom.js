import axios from 'axios'
import { showShackbars } from './actionSnackbarsInfo'
import {
  SET_USERDATE,
  SHOW_ADD_EDIT_MODAL,
  HIDE_ADD_EDIT_MODAL,
  SET_TASK_TO_STORE,
  SET_DATA_FROM_BASE,
  SET_DATE_FOR_NEW_TASK,
  SET_TIME_FOR_NEW_TASK,
  SET_IS_SHOW_ONLY_TASK
} from './actionTypes'

import {getNameDateWeekFromDate,formatDateYMDFromDate,calcTime} from './helpers'


export function getDataForWeek(date, userId) {
  return async (dispatch) => {
    await dispatch(loginSuccess(date))
    dispatch(getDataFromServer(userId))
  }
}

const loginSuccess = (userDate) => {
  return {
    type: SET_USERDATE,
    userDate,
  }
}

export function getDataFromServer(userId) {
  return async (dispatch) => {
    try {
      const response = await axios.get(
        `https://meet-room-3b283-default-rtdb.firebaseio.com/${userId}.json`
      )
      dispatch(setDataFromBaseToState(response.data))
    } catch (error) {
      dispatch(showShackbars(error, 'error'))
    }
  }
}

const setDataFromBaseToState = (dataFromBase) => {
  return {
    type: SET_DATA_FROM_BASE,
    dataFromBase,
  }
}

export function addEditModal(typeCart,dayAndDateWeek=null, taskTime=null, taskText=null) {
  return async (dispatch) => {
    let nameDayWeek = null
    let dateDayWeek = null
    if (dayAndDateWeek){
      const arrDayWeek = dayAndDateWeek.split(' ')
      nameDayWeek = arrDayWeek[0]
      dateDayWeek = arrDayWeek[1]         
    }
    dispatch(showAddEditModal(typeCart,nameDayWeek, dateDayWeek, taskTime, taskText))
  }
}

const showAddEditModal = (typeCart,nameDayWeek, dateDayWeek, taskTime, taskText) => {
  return {
    type: SHOW_ADD_EDIT_MODAL,
    typeCart,
    nameDayWeek,
    dateDayWeek,
    taskTime,
    taskText,
  }
}

export function closeAddEditTaskModal() {
  return {
    type: HIDE_ADD_EDIT_MODAL,
  }
}

export function setTextTaskToStore(taskText) {
  return {
    type: SET_TASK_TO_STORE,
    taskText,
  }
}

export function saveTask(userId, nameDayWeek, dateDayWeek, taskTime, taskText) {
  return async (dispatch) => {
    if (!nameDayWeek || !dateDayWeek || !taskTime || !taskText){
      dispatch(showShackbars('all fields are required', 'error'))
      return false
    }
    const taskData = { userId, nameDayWeek, dateDayWeek, taskTime, taskText }
    try {
      await axios.post(
        `https://meet-room-3b283-default-rtdb.firebaseio.com/${userId}.json`,
        taskData
      )
      dispatch(closeAddEditTaskModal())
      dispatch(getDataFromServer(userId))
      dispatch(showShackbars('Task saved', 'success'))
    } catch (error) {
      dispatch(showShackbars(error, 'error'))
    }
  }
}

export function setDateForNewTask(dateForNewTask,dataFromBase){
  
  const dateForFind = formatDateYMDFromDate(new Date(dateForNewTask), 'd.m.Y')
  const busyTime = []
  if (dataFromBase) {      
    Object.entries(dataFromBase).forEach((item, index) => {
      if (dateForFind === item[1].dateDayWeek){
           busyTime.push(item[1].taskTime)
      }
    })
  }    
  const arrayFreeTime = []
  let startTime = '00:00'
  while (startTime !== '23:30') {
    if (!busyTime.includes(startTime)){
      arrayFreeTime.push(startTime)
    }
    startTime = calcTime(startTime)
  }    
  return async (dispatch) => {
    dispatch(setDateForNewTaskToStore(dateForNewTask,arrayFreeTime))
  }
}

function setDateForNewTaskToStore(dateDayWeek,arrayFreeTime){
  const nameDayWeek =  getNameDateWeekFromDate(new Date(dateDayWeek))
  return {
    type: SET_DATE_FOR_NEW_TASK,
    dateDayWeek,nameDayWeek,arrayFreeTime
  }
} 

export function setUserTimeForNewTask(timeForNewTask){
  return async (dispatch) => {
    dispatch(setTimeForNewTaskToStore(timeForNewTask))
  }
}


function setTimeForNewTaskToStore(timeForNewTask){
  return {
    type: SET_TIME_FOR_NEW_TASK,
    timeForNewTask
  }
}

export function handleShowOnlyTask(isShowOnlyTask){
  return {
    type: SET_IS_SHOW_ONLY_TASK,
    isShowOnlyTask
  }
}


