export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_LOGOUT = 'LOGIN_LOGOUT'
export const LOGIN_ERROR = 'LOGIN_ERROR'
export const SHOW_SHACKBARS = 'SHOW_SHACKBARS'
export const CLOSE_SHACKBARS = 'CLOSE_SHACKBARS'
export const SET_USERDATE = 'SET_USERDATE'
export const SHOW_ADD_EDIT_MODAL = 'SHOW_ADD_EDIT_MODAL'
export const HIDE_ADD_EDIT_MODAL = 'HIDE_ADD_EDIT_MODAL'
export const SET_TASK_TO_STORE = 'SET_TASK_TO_STORE'
export const SET_DATA_FROM_BASE = 'SET_DATA_FROM_BASE'
export const SET_DATE_FOR_NEW_TASK ='SET_DATE_FOR_NEW_TASK'
export const SET_TIME_FOR_NEW_TASK = 'SET_TIME_FOR_NEW_TASK'
export const SET_IS_SHOW_ONLY_TASK = 'SET_IS_SHOW_ONLY_TASK'
