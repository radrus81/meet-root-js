import { LOGIN_SUCCESS, LOGIN_LOGOUT } from './actionTypes'
import axios from 'axios'
import { showShackbars } from './actionSnackbarsInfo'

export function checkLoginAndPassword(email, password) {
  return async (dispatch) => {
    const authData = {
      email,
      password,
      returnSecureToken: true,
    }
    try {
      let response = await axios.post(
        'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDMTe5vhMZuXWJcZOsgWiqMmIb7CTWr9HM',
        authData
      )
      setUserDataToLocalStorage(response.data)
      dispatch(loginSuccess(response.data))
    } catch (error) {
      console.log(error)
      dispatch(showShackbars(error.response.data.error.message, 'error'))
    }
  }
}

const loginSuccess = (userData) => {
  return {
    type: LOGIN_SUCCESS,
    userData,
  }
}

const setUserDataToLocalStorage = (userData) => {
  const userKeys = Object.keys(userData)
  userKeys.forEach((key) => {
    localStorage.setItem(key, userData[key])
  })
  const expirationDate = new Date(
    new Date().getTime() + userData.expiresIn * 1000
  )
  localStorage.setItem('expirationDate', expirationDate)
}

export function autoAuthorization() {
  const userData = {}
  for (let i = 0; i <= localStorage.length; i++) {
    let nameKey = localStorage.key(i)
    if (nameKey) {
      userData[nameKey] = localStorage.getItem(nameKey)
    }
  }
  return (dispatch) => {
    if (Object.keys(userData).length) {
      const expirationDate = new Date(localStorage.getItem('expirationDate'))
      if (expirationDate <= new Date()) {
        dispatch(logout())
      } else {
        dispatch(loginSuccess(userData))
      }
    } else {
      dispatch(logout())
    }
  }
}

export const logout = () => {
  localStorage.clear()
  return {
    type: LOGIN_LOGOUT,
  }
}
