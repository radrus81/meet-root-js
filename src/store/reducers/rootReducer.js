import { combineReducers } from 'redux'
import authorization from './authorizationReducer'
import shackbars from './snackbarsInfoReducer'
import meetroom from './meetroomReducer'

export default combineReducers({
  authorization,
  shackbars,
  meetroom,
})
