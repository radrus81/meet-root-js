import { LOGIN_SUCCESS, LOGIN_LOGOUT } from '../actions/actionTypes'

const initialState = {
  isAuthorization: false,
  userId: null,
  idToken: null,
  email: null,
  refreshToken: null,
  expirationDate: null,
}

export default function authorizationReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        isAuthorization: true,
        userId: action.userData.localId,
        idToken: action.userData.idToken,
        email: action.userData.email,
        refreshToken: action.userData.refreshToken,
        expirationDate: action.userData.expirationDate,
      }
    case LOGIN_LOGOUT:
      return initialState

    default:
      return state
  }
}
