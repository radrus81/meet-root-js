import {
  SET_USERDATE,
  SHOW_ADD_EDIT_MODAL,
  HIDE_ADD_EDIT_MODAL,
  SET_TASK_TO_STORE,
  SET_DATA_FROM_BASE,
  SET_DATE_FOR_NEW_TASK,
  SET_TIME_FOR_NEW_TASK,
  SET_IS_SHOW_ONLY_TASK
} from '../actions/actionTypes'
import { formatDateYMDFromDate } from '../actions/helpers'

const currentDate = () => {
  return formatDateYMDFromDate(new Date(), 'Y-m-d')
}

const initialState = {
  dataFromBase: null,
  userDate: currentDate(),
  isOpenAddEditTaskModal: false,
  typeCart:null,
  nameDayWeek: null,
  dateDayWeek: null,
  taskTime: null,
  taskText: '',
  freeTime:[],
  isShowOnlyTask:false
}

export default function meetroomReducer(state = initialState, action) {
  switch (action.type) {
    case SET_DATA_FROM_BASE:
      return {
        ...state,
        dataFromBase: action.dataFromBase,
      }
    case SET_USERDATE:
      return {
        ...state,
        userDate: action.userDate,
      }
    case SHOW_ADD_EDIT_MODAL:
      return {
        ...state,
        isOpenAddEditTaskModal: true,
        typeCart:action.typeCart,
        nameDayWeek: action.nameDayWeek,
        dateDayWeek: action.dateDayWeek,
        taskTime: action.taskTime,
        taskText: action.taskText,
      }
    case HIDE_ADD_EDIT_MODAL:
      return {
        ...state,
        isOpenAddEditTaskModal: false,
        typeCart:null,
        nameDayWeek: null,
        dateDayWeek: null,
        taskTime: null,
        taskText: '',
      }
    case SET_TASK_TO_STORE:
      return {
        ...state,
        taskText: action.taskText,
      }
    case SET_DATE_FOR_NEW_TASK:
      return {
        ...state,
        nameDayWeek: action.nameDayWeek,
        dateDayWeek: formatDateYMDFromDate(new Date(action.dateDayWeek), 'd.m.Y'),
        freeTime: action.arrayFreeTime
      }
    case SET_TIME_FOR_NEW_TASK:
      return {
        ...state,
        taskTime:action.timeForNewTask
      }
    case SET_IS_SHOW_ONLY_TASK:
      return {
        ...state,
        isShowOnlyTask:action.isShowOnlyTask
      }

    default:
      return state
  }
}
