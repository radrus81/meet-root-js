import React from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import Container from '@material-ui/core/Container'
import { Formik, Form } from 'formik'
import RegistrationFormSchema from '../Registration/RegistrationFormSchema'
import useStyles from '../Authorization/AuthorizationStyles'
import FormikField from '../ui/FormikField'
import SignLink from '../ui/SignLink'
import FormikButton from '../ui/FormikButton'
import FormikHeader from '../ui/FormikHeader'

const Registration = ({ userRegistration, history }) => {
  const classes = useStyles()
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <FormikHeader title="Sign up" />
        <Formik
          initialValues={{
            email: '',
            password: '',
            confirmPassword: '',
          }}
          validationSchema={RegistrationFormSchema}
          onSubmit={async (values, { setSubmitting }) => {
            userRegistration(values.email, values.password)
            setSubmitting(false)
          }}
        >
          {({ errors, touched, values, isSubmitting }) => (
            <Form>
              <FormikField
                error={errors.email}
                touch={touched.email}
                value={values.email}
                name="email"
                label="E-mail"
              />
              <FormikField
                error={errors.password}
                touch={touched.password}
                value={values.password}
                name="password"
                type="password"
              />
              <FormikField
                error={errors.confirmPassword}
                touch={touched.confirmPassword}
                value={values.confirmPassword}
                name="confirmPassword"
                label="confirm password"
                type="password"
              />
              <FormikButton
                classes={classes}
                isSubmitting={isSubmitting}
                btnTitle="Sing Up"
              />
              <SignLink
                history={history}
                link="/"
                linkTitle="Already have an account? Sign in"
              />
            </Form>
          )}
        </Formik>
      </div>
    </Container>
  )
}

export default Registration
