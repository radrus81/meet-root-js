import * as Yup from 'yup'

const RegistrationFormSchema = Yup.object().shape({
  email: Yup.string().required('Login required'),
  password: Yup.string()
    .min(6, 'The password must be 6 characters long')
    .required('Password required'),
  confirmPassword: Yup.string()
    .required()
    .oneOf([Yup.ref('password'), null], 'Passwords must match'),
})

export default RegistrationFormSchema
