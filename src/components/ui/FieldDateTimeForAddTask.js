import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

const useStyles = makeStyles((theme) => ({  
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
      }, 
    textField: {
      margin: theme.spacing(1),
      width: 200,
    },   
  }))

const FieldDateTimeForAddTask = ({freeTime,setDateForNewTask,setUserTimeForNewTask}) =>{
    const classes = useStyles()
    
    return (
        <>
        <TextField
            id="date"
            label="Select date"
            type="date"
            defaultValue='25.02.2020'
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
            onChange={(event) => {setDateForNewTask(event.target.value)}}
          />
          <FormControl className={classes.formControl}>
            <InputLabel shrink id="demo-simple-select-placeholder-label-label">
                Select time
                </InputLabel>
            <Select
                    labelId="demo-simple-select-autowidth-label"
                    id="demo-simple-select-autowidth"          
                    onChange={(event)=>{setUserTimeForNewTask(event.target.value)}}                           
                >
                {                    
                    freeTime.map((time)=>{
                        return <MenuItem value={time}>{time}</MenuItem>
                    })
                }         
                                
            </Select>
        </FormControl>
        </>
    )
}

export default FieldDateTimeForAddTask