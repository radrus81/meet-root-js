import React from 'react'
import { Field } from 'formik'
import { TextField } from 'material-ui-formik-components/TextField'

const FormikField = ({
  error,
  touch,
  value,
  name,
  type = 'text',
  label = name,
}) => {
  return (
    <Field
      component={TextField}
      error={error && touch}
      variant="outlined"
      margin="normal"
      fullWidth
      name={name}
      label={label}
      type={type}
      id={name}
      value={value || ''}
    />
  )
}

export default FormikField
