import React from 'react'
import Button from '@material-ui/core/Button'

const FormikButton = ({ classes, isSubmitting, btnTitle }) => {
  return (
    <Button
      type="submit"
      fullWidth
      variant="contained"
      color="primary"
      className={classes.submit}
      disabled={isSubmitting}
    >
      {btnTitle}
    </Button>
  )
}

export default FormikButton
