import React from 'react'
import Link from '@material-ui/core/Link'
import Grid from '@material-ui/core/Grid'

const SignLink = ({ history, link, linkTitle }) => {
  return (
    <Grid container justify="flex-end">
      <Grid item>
        <Link
          href="#"
          variant="body2"
          onClick={(event) => {
            event.preventDefault()
            history.push(link)
          }}
        >
          {linkTitle}
        </Link>
      </Grid>
    </Grid>
  )
}

export default SignLink
