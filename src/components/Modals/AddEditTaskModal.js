import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import CardTask from './CardTask'

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

 const AddEditTaskModal = ({isOpenAddEditTaskModal,typeCart,nameDayWeek,dateDayWeek,
                      taskTime,taskText,closeAddEditTaskModal,setTextTaskToStore,saveTask}) => {
  const classes = useStyles();  
 
  return (
    <div>     
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        className={classes.modal}
        open={isOpenAddEditTaskModal}
        onClose={closeAddEditTaskModal}        
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={isOpenAddEditTaskModal}>
          <div>
            <CardTask 
                typeCart={typeCart}
                nameDayWeek = {nameDayWeek}
                dateDayWeek={dateDayWeek}
                taskTime={taskTime}
                taskText={taskText}
                setTextTaskToStore={setTextTaskToStore}
                saveTask={saveTask}
                />
          </div>
        </Fade>
      </Modal>
    </div>
  );
}

export default AddEditTaskModal