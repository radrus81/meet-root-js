import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import useStyles from './NavbarStyles'
import ProfileButton from './ProfileButton'

export default function Navbar({ isAuthorization, email, logout }) {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          ></IconButton>
          <Typography variant="h6" className={classes.title}>
            MeetRoom
          </Typography>
          {isAuthorization ? (
            <ProfileButton email={email} logout={logout} />
          ) : null}
        </Toolbar>
      </AppBar>
    </div>
  )
}
