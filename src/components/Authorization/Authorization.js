import React from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import Container from '@material-ui/core/Container'
import { Formik, Form } from 'formik'
import AuthorizationFormSchema from './AuthorizationFormSchema'
import useStyles from './AuthorizationStyles'
import FormikField from '../ui/FormikField'
import SignLink from '../ui/SignLink'
import FormikButton from '../ui/FormikButton'
import FormikHeader from '../ui/FormikHeader'

const Authorization = ({ checkLoginAndPassword, history }) => {
  const classes = useStyles()
  return (
    <Container>
      <CssBaseline />
      <div className={classes.paper}>
        <FormikHeader title="Sign in" />
        <Formik
          initialValues={{
            email: '',
            password: '',
          }}
          validationSchema={AuthorizationFormSchema}
          onSubmit={async (values, { setSubmitting }) => {
            checkLoginAndPassword(values.email, values.password)
            setSubmitting(false)
          }}
        >
          {({ errors, touched, values, isSubmitting }) => (
            <Form>
              <FormikField
                error={errors.email}
                touch={touched.email}
                value={values.email}
                name="email"
                label="E-mail"
              />
              <FormikField
                error={errors.password}
                touch={touched.password}
                value={values.password}
                name="password"
                type="password"
              />
              <FormikButton
                classes={classes}
                isSubmitting={isSubmitting}
                btnTitle="Sing In"
              />
              <SignLink
                history={history}
                link="/registration"
                linkTitle="Don't have an account? Sign Up"
              />
            </Form>
          )}
        </Formik>
      </div>
    </Container>
  )
}

export default Authorization
