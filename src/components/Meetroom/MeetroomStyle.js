import { makeStyles } from '@material-ui/core/styles'

const MeetroomStyle = makeStyles((theme) => ({
  root: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
  },
  container: {
    maxHeight: '75vh',
    marginTop:'15px'
  },
  tableCell:{    
    padding:'7px'
  },
  taskBox: {
    width: '150px',
    border: '1px solid #ccc',
    padding: '3px',
    borderRadius: '5px',
    height: '100px',
    cursor: 'pointer',
    wordWrap: 'break-word',       
  },
}))

export default MeetroomStyle
