import React, { useEffect } from 'react'
import Paper from '@material-ui/core/Paper'
import Table from '@material-ui/core/Table'
import CssBaseline from '@material-ui/core/CssBaseline'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableContainer from '@material-ui/core/TableContainer';
import Box from '@material-ui/core/Box'
import meetroomStyle from './MeetroomStyle'
import TableHeader from './TableHeader'
import TableRows from './TableRows'
import MeetroomDate from '../../containers/Meetroom/MeetroomDate'

const Meetroot = ({
  dataFromBase,
  userDate,
  addEditModal,
  getDataFromServer,
  isShowOnlyTask
}) => {
  const classes = meetroomStyle()
  const arrayTableHeader = TableHeader(userDate)
  const tableHeader = arrayTableHeader[1]
  const daysWeek = arrayTableHeader[0]
  const tableRows = TableRows(dataFromBase, daysWeek[0],isShowOnlyTask)

  useEffect(() => {
    getDataFromServer()
  }, [])

  return (
    <Paper className={classes.root}>
      <CssBaseline />
      <MeetroomDate />
      <TableContainer className={classes.container}>
      <Table stickyHeader aria-label="sticky table">
        <TableHead>
          <TableRow>
            {tableHeader.map((column) => (
              <TableCell
                key={column.id}
                align={column.align}
                style={{ minWidth: column.minWidth }}
              >
                {column.label}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {tableRows.map((row) => {
            return (
              <TableRow hover role="checkbox" tabIndex={-1} key={row.time}>
                {tableHeader.map((column) => {
                  let value = row[column.id]
                  return (
                    <TableCell key={column.id} align={column.align}  className={classes.tableCell}>
                      {column.label !== 'Time' ? (
                        <Box
                          color="text.primary"
                          p={2}
                          className={classes.taskBox}
                          onClick={() => {
                            addEditModal(
                              'edit',
                              column.label,
                              value.substr(0, 5),
                              value.substr(5)
                            )
                          }}
                          title={value.substr(0, 5)}
                        >
                          {value.substr(5).length < 35
                            ? value.substr(5)
                            : `${value.substr(5, 35)}...`}
                        </Box>
                      ) : (
                        value
                      )}
                    </TableCell>
                  )
                })}
              </TableRow>
            )
          })}
        </TableBody>
      </Table>
      </TableContainer>
    </Paper>
  )
}

export default Meetroot
