import { calcTime } from '../../store/actions/helpers'

const getTaskText = (labelTime, labelDayWeek, daysWeek, dataFromBase) => {
  const day = daysWeek[labelDayWeek] // 21.12.2020
  let taskText = ''
  if (dataFromBase) {
    Object.entries(dataFromBase).forEach((item, index) => {
      if (item[1].taskTime === labelTime && day === item[1].dateDayWeek)
        taskText = item[1].taskText
    })
  }
  return taskText
}

const TableRows = (dataFromBase, daysWeek,isShowOnlyTask) => {  
  const dataRows = []
  let startTime = '00:00'
  while (startTime !== '23:30') {
    
    const mondayTaskText = `${getTaskText(
      startTime,
      'monday',
      daysWeek,
      dataFromBase
    )}`
    const tuesdayTaskText = `${getTaskText(
      startTime,
      'tuesday',
      daysWeek,
      dataFromBase
    )}` 
    const wednesdayTaskText=`${getTaskText(
      startTime,
      'wednesday',
      daysWeek,
      dataFromBase
    )}`
    const thursdayTaskText=`${getTaskText(
      startTime,
      'thursday',
      daysWeek,
      dataFromBase
    )}`
    const fridayTaskText=`${getTaskText(
      startTime,
      'friday',
      daysWeek,
      dataFromBase
    )}`    
    const saturdayTaskText=`${getTaskText(
      startTime,
      'saturday',
      daysWeek,
      dataFromBase
    )}`
    const sundayTaskText=`${getTaskText(
      startTime,
      'sunday',
      daysWeek,
      dataFromBase
    )}`

  if (isShowOnlyTask){     
    if (mondayTaskText || tuesdayTaskText || wednesdayTaskText || thursdayTaskText 
      || fridayTaskText || saturdayTaskText || sundayTaskText){        
      dataRows.push({
        time: startTime,
        monday: `${startTime}${mondayTaskText}`,
        tuesday: `${startTime}${tuesdayTaskText}`,
        wednesday: `${startTime}${wednesdayTaskText}`,
        thursday: `${startTime}${thursdayTaskText}`,
        friday: `${startTime}${fridayTaskText}`,      
        saturday: `${startTime}${saturdayTaskText}`,
        sunday: `${startTime}${sundayTaskText}`,
      })
    }
  }else{     
      dataRows.push({
        time: startTime,
        monday: `${startTime}${mondayTaskText}`,
        tuesday: `${startTime}${tuesdayTaskText}`,
        wednesday: `${startTime}${wednesdayTaskText}`,
        thursday: `${startTime}${thursdayTaskText}`,
        friday: `${startTime}${fridayTaskText}`,      
        saturday: `${startTime}${saturdayTaskText}`,
        sunday: `${startTime}${sundayTaskText}`,
      })
    }

    startTime = calcTime(startTime)
  }
  return dataRows
}
export default TableRows
