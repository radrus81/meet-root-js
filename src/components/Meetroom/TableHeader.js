import { formatDateYMDFromDate } from '../../store/actions/helpers'

const getMonday = (d) => {
  d = new Date(d)
  var day = d.getDay(),
    diff = d.getDate() - day + (day === 0 ? -6 : 1) // adjust when day is sunday
  return new Date(d.setDate(diff))
}

const formatDate = (date, countDay) => {
  date.setDate(date.getDate() + countDay)
  return formatDateYMDFromDate(date, 'd.m.Y')
}

const TableHeader = (userDate) => {
  const monday = formatDate(getMonday(userDate), 0)
  const tuesday = formatDate(getMonday(userDate), 1)
  const wednesday = formatDate(getMonday(userDate), 2)
  const thursday = formatDate(getMonday(userDate), 3)
  const friday = formatDate(getMonday(userDate), 4)
  const saturday = formatDate(getMonday(userDate), 5)
  const sunday = formatDate(getMonday(userDate), 6)

  return [
    [{ monday, tuesday, wednesday, thursday, friday, saturday, sunday }],
    [
      { id: 'time', label: 'Time', minWidth: 70 },
      {
        id: 'monday',
        label: `Monday ${monday}`,
        minWidth: 100,
      },
      {
        id: 'tuesday',
        label: `Tuesday ${tuesday}`,
        minWidth: 100,
      },
      {
        id: 'wednesday',
        label: `Wednesday ${wednesday}`,
        minWidth: 100,
      },
      {
        id: 'thursday',
        label: `Thursday ${thursday}`,
        minWidth: 100,
      },
      {
        id: 'friday',
        label: `Friday ${friday}`,
        minWidth: 100,
      },
      {
        id: 'saturday',
        label: `Saturday ${saturday}`,
        minWidth: 100,
      },
      {
        id: 'sunday',
        label: `Sunday ${sunday}`,
        minWidth: 100,
      },
    ],
  ]
}

export default TableHeader
