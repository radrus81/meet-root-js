import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  wrapper: {
    marginTop: '15px',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
  btnAddTask: {
    marginLeft: '25px',
    marginTop: '10px',
  },
  checkBoxTask: {
    marginTop: '10px',
    marginLeft: '25px',
  },
}))

const MeetroomDate = ({
  userDate,
  isShowOnlyTask,
  getDataForWeek,
  addEditModal,
  handleShowOnlyTask,
}) => {
  const classes = useStyles()

  return (
    <form className={classes.container} noValidate>
      <div className={classes.wrapper}>
        <TextField
          id="date"
          label="Select date"
          type="date"
          defaultValue={userDate}
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          onChange={(event) => {
            getDataForWeek(event.target.value)
          }}
        />
        <Button
          variant="contained"
          color="primary"
          className={classes.btnAddTask}
          onClick={() => {
            addEditModal('add')
          }}
        >
          Add task
        </Button>
        <FormControlLabel
          control={
            <Checkbox
              className={classes.checkBoxTask}
              checked={isShowOnlyTask}
              onChange={(event) => {
                handleShowOnlyTask(event.target.checked)
              }}
              name="showOnlyTask"
              color="primary"
            />
          }
          label="Show only periods with meetings"
        />
      </div>
    </form>
  )
}

export default MeetroomDate
