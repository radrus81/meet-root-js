import { connect } from 'react-redux'
import Registration from '../components/Registration/Registration'
import { userRegistration } from '../store/actions/actionRegistration'

const mapStateToProps = (state) => ({
  // errorMessage: state.authorization.errorMessage,
})

const mapDispatchToProps = (dispatch) => ({
  userRegistration: (email, password) =>
    dispatch(userRegistration(email, password)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Registration)
