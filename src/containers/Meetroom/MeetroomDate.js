import { connect } from 'react-redux'
import MeetroomDate from '../../components/Meetroom/MeetroomDate'
import { getDataForWeek,addEditModal,handleShowOnlyTask } from '../../store/actions/actionMeetroom'

const mapStateToProps = state => ({ 
  userId: state.authorization.userId, 
  userDate: state.meetroom.userDate,
  isShowOnlyTask:state.meetroom.isShowOnlyTask
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { userId } = stateProps
  const { dispatch } = dispatchProps

  return {
      ...stateProps,
      getDataForWeek: (userDate) => dispatch(getDataForWeek(userDate,userId)),
      addEditModal: () => dispatch(addEditModal()), 
      handleShowOnlyTask:(isShowOnlyTask)=>dispatch(handleShowOnlyTask(isShowOnlyTask))     
  }
}

export default connect(mapStateToProps, null, mergeProps)(MeetroomDate)