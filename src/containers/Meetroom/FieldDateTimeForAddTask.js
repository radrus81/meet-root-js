import { connect } from 'react-redux'
import FieldDateTimeForAddTask from '../../components/ui/FieldDateTimeForAddTask'
import { setDateForNewTask,setUserTimeForNewTask } from '../../store/actions/actionMeetroom'

const mapStateToProps = state => ({   
  userDate: state.meetroom.userDate,
  dataFromBase: state.meetroom.dataFromBase,
  freeTime:state.meetroom.freeTime
})

const mergeProps = (stateProps, dispatchProps, ownProps) => { 
  const { dispatch } = dispatchProps
  const { dataFromBase } = stateProps

  return {
      ...stateProps,    
      setDateForNewTask: (dateForNewTask) => dispatch(setDateForNewTask(dateForNewTask,dataFromBase)), 
      setUserTimeForNewTask: (timeForNewTask) => dispatch(setUserTimeForNewTask(timeForNewTask)),      
  }
}

export default connect(mapStateToProps, null, mergeProps)(FieldDateTimeForAddTask)