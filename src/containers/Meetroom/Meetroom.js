import { connect } from 'react-redux'
import Meetroom from '../../components/Meetroom/Meetroom'
import {addEditModal,getDataFromServer} from '../../store/actions/actionMeetroom'

const mapStateToProps = state => ({
  dataFromBase: state.meetroom.dataFromBase,
  userId: state.authorization.userId,
  userDate: state.meetroom.userDate,
  isShowOnlyTask:state.meetroom.isShowOnlyTask
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { userId } = stateProps
  const { dispatch } = dispatchProps

  return {
      ...stateProps,
      addEditModal: (typeCart,dayAndDateWeek,taskTime,taskText) =>
      dispatch(addEditModal(typeCart,dayAndDateWeek,taskTime,taskText)),
      getDataFromServer:()=>{dispatch(getDataFromServer(userId))} 
  }
}

export default connect(mapStateToProps, null, mergeProps)(Meetroom)