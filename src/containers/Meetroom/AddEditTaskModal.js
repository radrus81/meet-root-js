import { connect } from 'react-redux'
import AddEditTaskModal from '../../components/Modals/AddEditTaskModal'
import {closeAddEditTaskModal,setTextTaskToStore,saveTask} from '../../store/actions/actionMeetroom'

const mapStateToProps = state => ({
    userId: state.authorization.userId,
    isOpenAddEditTaskModal: state.meetroom.isOpenAddEditTaskModal,
    typeCart:state.meetroom.typeCart,
    nameDayWeek: state.meetroom.nameDayWeek,
    dateDayWeek:state.meetroom.dateDayWeek,
    taskTime:state.meetroom.taskTime,
    taskText:state.meetroom.taskText
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const { userId,nameDayWeek,dateDayWeek,taskTime,taskText} = stateProps
    const { dispatch } = dispatchProps

    return {
        ...stateProps,
        closeAddEditTaskModal: () =>dispatch(closeAddEditTaskModal()),
        setTextTaskToStore:(taskText)=>dispatch(setTextTaskToStore(taskText)),
        saveTask:()=>dispatch(saveTask(userId,nameDayWeek,dateDayWeek,taskTime,taskText)) 
    }
}

export default connect(mapStateToProps, null, mergeProps)(AddEditTaskModal)

