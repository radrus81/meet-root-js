import { connect } from 'react-redux'
import Navbar from '../components/Navbar/Navbar'
import { logout } from '../store/actions/actionAuthorization'

const mapStateToProps = (state) => ({
  isAuthorization: state.authorization.isAuthorization,
  email: state.authorization.email,
})

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logout()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Navbar)
