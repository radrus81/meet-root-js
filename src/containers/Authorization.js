import { connect } from 'react-redux'
import Authorization from '../components/Authorization/Authorization'
import { checkLoginAndPassword } from '../store/actions/actionAuthorization'

const mapDispatchToProps = (dispatch) => ({
  checkLoginAndPassword: (email, password) =>
    dispatch(checkLoginAndPassword(email, password)),
})

export default connect(null, mapDispatchToProps)(Authorization)
