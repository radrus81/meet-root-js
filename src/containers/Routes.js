import React from 'react'
import { connect } from 'react-redux'
import { Route, Switch, withRouter, Redirect } from 'react-router-dom'

import Authorization from './Authorization'
import Registration from './Registration'
import Meetroom from '../containers/Meetroom/Meetroom'

const Routes = ({ isAuthorization }) => {
  let routes
  isAuthorization
    ? (routes = (
        <Switch>
          <Route component={Meetroom} path="/meetroom" />
          <Redirect from="/" to="/meetroom" />
        </Switch>
      ))
    : (routes = (
        <Switch>
          <Route component={Registration} path="/registration" />
          <Route component={Authorization} path="/" exact />
          <Redirect to="/" />
        </Switch>
      ))
  return <div className="container">{routes}</div>
}

const mapStateToProps = (state) => ({
  isAuthorization: state.authorization.isAuthorization,
})

export default withRouter(connect(mapStateToProps, null)(Routes))
